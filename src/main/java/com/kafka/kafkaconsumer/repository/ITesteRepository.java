package com.kafka.kafkaconsumer.repository;

import org.springframework.data.repository.CrudRepository;

import com.kafka.kafkaconsumer.model.Teste;

public interface ITesteRepository extends CrudRepository<Teste, Integer>{

}
