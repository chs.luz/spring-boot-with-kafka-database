package com.kafka.kafkaconsumer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kafka.kafkaconsumer.model.Teste;
import com.kafka.kafkaconsumer.repository.ITesteRepository;

@Service
public class TesteServiceImpl implements TesteService {
	
	@Autowired
	ITesteRepository iTesteRepository;

	@Override
	public void save(Teste teste) {
		iTesteRepository.save(teste);	
	}
	
}
