package com.kafka.kafkaconsumer.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.kafka.kafkaconsumer.config.TenantContext;
import com.kafka.kafkaconsumer.model.Teste;
import com.kafka.kafkaconsumer.service.TesteService;

@Component
public class KafkaConsumer {
	
	@Autowired
	TesteService testeService;
	
	private static Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);

    @KafkaListener(topics = { "kakfa-teste" })
    public void onMessage(ConsumerRecord<String, String> record) {
        try {
        	String tenant = "teste1";
        	TenantContext.setCurrentTenant(tenant);
			Teste teste = new Teste(record.value());
			testeService.save(teste);
			logger.info("mensagem consumida.");
		} catch (Exception e) {
			logger.error("Erro ao consumir mensagem",e);
		}
    }

}
